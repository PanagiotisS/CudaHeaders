# README #

Some CUDA headers for common tasks.

### Files ###

- **GpuTimer.h**  : Time GPU event
- **GPUassert.h** : A CUDA commands wrapper in order to check for errors.

### Contribution guidelines ###

* Other useful headers. Maybe create for other languages too?
* Corrections.
