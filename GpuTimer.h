/** A time header to time the GPU events.
 * It can also, potentially, time the CPU. However, it needs a synchronize
 *  event when the event start takes place
 * Be careful, when to call the destructor. E.g. calling cudaDeviceReset
 *  before calling the destructor might lead to errors.
 *  Maybe, a good practice is to define a scope before creating the timer
 *  and close the scope in order to call the destructor after calculating
 *  the elapsed time.
 *      E.g.
 *          { // creating the scope
 *          GpuTimer timer;
 *          timer.Start();
 *          mplamplampla
 *          timer.Stop();
 *          printf("Time elapsed = %g ms\n", timer.Elapsed());
 *          } // closing the scope and calling the destructor
 */
#ifndef __GPU_TIMER_H__
#define __GPU_TIMER_H__
#include "GPUassert.h"

struct GpuTimer
{
      cudaEvent_t start;
      cudaEvent_t stop;

      GpuTimer()
      {
            GPUerrchk(cudaEventCreate(&start));
            GPUerrchk(cudaEventCreate(&stop));
      }

      ~GpuTimer()
      {
            GPUerrchk(cudaEventDestroy(start));
            GPUerrchk(cudaEventDestroy(stop));
      }

      void Start()
      {
            GPUerrchk(cudaEventRecord(start, 0));
      }

      void Stop()
      {
            GPUerrchk(cudaEventRecord(stop, 0));
      }

      float Elapsed()
      {
            float elapsed;
            GPUerrchk(cudaEventSynchronize(stop));
            GPUerrchk(cudaEventElapsedTime(&elapsed, start, stop));
            return elapsed;
      }
};

#endif  /* __GPU_TIMER_H__ */
