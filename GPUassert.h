/** A wrapper in order to for errors.
 * http://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
 */
#ifndef GPUerrchk
#ifndef GPUassert
#define GPUerrchk(ans) { GPUassert((ans), __FILE__, __LINE__); }

inline void GPUassert(cudaError_t code, char * file, int line, bool Abort=true)
{
    if (code != 0) {
        fprintf(stderr,"GPUassert: %s %s %d\n",cudaGetErrorString(code),file,line);
        if (Abort) exit(code);
    }
}

#endif
#endif
